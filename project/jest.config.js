module.exports = {
    preset: 'jest-preset-angular',
    globalSetup: 'jest-preset-angular/global-setup',
    setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
    transform: {
        '^.+\\.(ts|html)$': [
            'jest-preset-angular',
            {
                tsconfig: '<rootDir>/tsconfig.spec.json',
                stringifyContentPathRegex: '\\.html$',
                esbuild: true,
            },
        ],
    },
    testMatch: ['**/+(*.)+(spec).+(ts)'],
    moduleFileExtensions: ['ts', 'html', 'js', 'json'],
    reporters: [
        'default',
        [
            'jest-junit',
            {
                outputName: 'junit.xml',
            },
        ],
    ],
};
