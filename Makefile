question:
	find . -name 'node_modules' -type d -prune -exec rm -rf '{}' +
	find . -name 'build' -type d -prune -exec rm -rf '{}' +
	find . -name 'dist' -type d -prune -exec rm -rf '{}' +
	find . -name '.gitignore' -type d -prune -exec rm -rf '{}' +
